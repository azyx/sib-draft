# no tags = --tags all
ansible-playbook tags.yml -i ~/ansible/inventory

# only mytags1
ansible-playbook tags.yml -i ~/ansible/inventory --tags mytags1

# only tasks that doesn't contain tags mytags1
ansible-playbook tags.yml -i ~/ansible/inventory --skip-tags mytags1

# only mytags1 tasks without mytags2
ansible-playbook tags.yml -i ~/ansible/inventory --tags mytags1 --skip-tags mytags2
