# to export inventory in YAML format
ansible-inventory --yaml --list

SIB_DRAFT=/git/SIB-draft
ansible-playbook $SIB_DRAFT/YAML/show-yaml-vars.yml
